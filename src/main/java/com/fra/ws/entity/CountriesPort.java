/**
 * CountriesPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.fra.ws.entity;

public interface CountriesPort extends java.rmi.Remote {
    public com.fra.ws.entity.GetCountryResponse getCountry(com.fra.ws.entity.GetCountryRequest getCountryRequest) throws java.rmi.RemoteException;
}
