package com.fra.ws.entity;

public class CountriesPortProxy implements com.fra.ws.entity.CountriesPort {
  private String _endpoint = null;
  private com.fra.ws.entity.CountriesPort countriesPort = null;
  
  public CountriesPortProxy() {
    _initCountriesPortProxy();
  }
  
  public CountriesPortProxy(String endpoint) {
    _endpoint = endpoint;
    _initCountriesPortProxy();
  }
  
  private void _initCountriesPortProxy() {
    try {
      countriesPort = (new com.fra.ws.entity.CountriesPortServiceLocator()).getCountriesPortSoap11();
      if (countriesPort != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)countriesPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)countriesPort)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (countriesPort != null)
      ((javax.xml.rpc.Stub)countriesPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.fra.ws.entity.CountriesPort getCountriesPort() {
    if (countriesPort == null)
      _initCountriesPortProxy();
    return countriesPort;
  }
  
  public com.fra.ws.entity.GetCountryResponse getCountry(com.fra.ws.entity.GetCountryRequest getCountryRequest) throws java.rmi.RemoteException{
    if (countriesPort == null)
      _initCountriesPortProxy();
    return countriesPort.getCountry(getCountryRequest);
  }
  
  
}