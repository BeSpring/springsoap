/**
 * CountriesPortService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.fra.ws.entity;

public interface CountriesPortService extends javax.xml.rpc.Service {
    public java.lang.String getCountriesPortSoap11Address();

    public com.fra.ws.entity.CountriesPort getCountriesPortSoap11() throws javax.xml.rpc.ServiceException;

    public com.fra.ws.entity.CountriesPort getCountriesPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
