package com.fra.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWsSoapApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWsSoapApplication.class, args);
	}

}

